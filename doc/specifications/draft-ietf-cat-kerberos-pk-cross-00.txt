
INTERNET-DRAFT                                              Brian Tung
draft-ietf-cat-kerberos-pk-cross-00.txt                 Tatyana Ryutov
Updates: RFC 1510                                      Clifford Neuman
expires May 25, 1997                                               ISI


    Public Key Cryptography for Cross-Realm Authentication in Kerberos


0.  Status Of this Memo

    This document is an Internet-Draft.  Internet-Drafts are working
    documents of the Internet Engineering Task Force (IETF), its
    areas, and its working groups.  Note that other groups may also
    distribute working documents as Internet-Drafts.

    Internet-Drafts are draft documents valid for a maximum of six
    months and may be updated, replaced, or obsoleted by other
    documents at any time.  It is inappropriate to use Internet-Drafts
    as reference material or to cite them other than as ``work in
    progress.''

    To learn the current status of any Internet-Draft, please check
    the ``1id-abstracts.txt'' listing contained in the Internet-Drafts
    Shadow Directories on ds.internic.net (US East Coast),
    nic.nordu.net (Europe), ftp.isi.edu (US West Coast), or
    munnari.oz.au (Pacific Rim).

    The distribution of this memo is unlimited.  It is filed as
    draft-ietf-cat-kerberos-pk-cross-00.txt, and expires May 25, 1997.
    Please send comments to the authors.

1.  Abstract

    This document defines extensions to the Kerberos protocol
    specification (RFC 1510, "The Kerberos Network Authentication
    Service (V5)", September 1993) to provide a method for using
    public key cryptography during cross-realm authentication.  The
    methods defined here specify the way in which message exchanges
    are to be used to transport cross-realm secret keys protected by
    encryption under public keys certified as belonging to KDCs.

2.  Motivation

    The advantages provided by public key cryptography--resistance to
    key attacks, ease of recoverability in the event of a compromise,
    the possibility of an autonomous authentication infrastructure, to
    name a few--have produced a demand for use by Kerberos
    authentication protocol.  A draft describing the use of public key
    cryptography in the initial authentication exchange in Kerberos
    has already been submitted.  This draft describes its use in
    cross-realm authentication.

    The principal advantage provided by public key cryptography in
    cross-realm authentication lies in the ability to leverage the
    existing public key infrastructure.  It frees the Kerberos realm
    administrator from having to maintain separate keys for each other
    realm with which it wishes to exchange authentication information,
    or to utilize a hierarchical arrangement, which may pose problems
    of trust.

    Even with the advent of chained cross-realm authentication, there
    must be some way to locate the path by which separate realms are
    to be transited.  The current method, which makes use of the
    DNS-like realm names typical to Kerberos, requires trust of the
    intermediate KDCs.

    The methods described in this draft allow a realm to specify, at
    the time of authentication, which certification paths it will
    trust.  A shared key for cross-realm authentication can be
    established, for a period of time.  Furthermore, these methods are
    transparent to the client, so that only the KDC's need to be
    modified to use them.

    It is not necessary to implement the changes described in the
    "Public Key Cryptography for Initial Authentication" draft to make
    use of the changes in this draft.  We solicit comments about the
    interaction between the two protocol changes, but as of this
    writing, the authors do not perceive any obstacles to using both.

3.  Protocol Emendments

    We assume that the user has already obtained a TGT.  To perform
    cross-realm authentication, the user sends a request to the local
    KDC as per RFC 1510.  If the two realms share a secret key, then
    cross-realm authentication proceeds as usual.  Otherwise, the
    local KDC may attempt to establish a shared key with the remote
    KDC using public key cryptography.

    We first address the case in which the local KDC has and trusts
    the remote KDC's public key certificate.  If it does not, then
    the two KDCs must exchange public keys, as described below in
    Section 3.2.

    We will consider the specific channel on which the message
    exchanges take place in Section 5 below.

3.1.  Shared Key Exchange

    The local KDC sends a message to the remote KDC containing the
    shared key to be used to perform cross-realm authentication,
    encrypted with the remote KDC's public key, and signed with
    the local KDC's private key:

        PKX-SHARED-KEY-SEND ::= [APPLICATION 31] SEQUENCE {
            signedKeyPack           [0] SignedKeyPack,
            pubKeyCert              [1] Certificate
        }

        SignedKeyPack ::= SEQUENCE {
            keyPack                 [0] KeyPack,
            sigKeyPack              [1] Signature
                                        -- of keyPack
        }

        KeyPack ::= SEQUENCE {
            encKeyData              [0] EncryptedData,
                                        -- of type KeyData
            timestamp               [1] KerberosTime,
            keyPackNonce            [2] INTEGER
        }

        KeyData ::= SEQUENCE {
            sharedKey               [0] EncryptionKey,
            keyExpiration           [1] KerberosTime OPTIONAL,
        }

        Signature ::= SEQUENCE {
            sigType                 [0] INTEGER,
            kvno                    [1] INTEGER OPTIONAL,
            signedHash              [2] OCTET STRING
        }

    where pubKeyCert contains the public key of the local KDC.  Its
    exact form will depend on the particular public key service used
    by the local KDC.

    Upon receiving of the PKX-SHARED-KEY-SEND, the remote KDC attempts
    to verify the pubKeyCert.  If it cannot verify the public key
    certificate, or if it finds the key unacceptable, the remote KDC
    returns an error message.  Otherwise, it returns a message to the
    local KDC, signed by the remote KDC's private key, acknowledging
    receipt of the shared key:

        PKX-SHARED-KEY-ACK ::= [APPLICATION 32] SEQUENCE {
            ackPack                 [0] AckPack,
            sigAckPack              [1] Signature
                                        -- of ackPack
        }

        AckPack ::= SEQUENCE {
            timestamp               [0] KerberosTime,
            keyPackNonce            [1] INTEGER,
                                        -- from type KeyPack
            ackNonce                [2] INTEGER
                                        -- new nonce
        }

    Since it is presumed that the local KDC has and trusts the remote
    KDC's public key, this key does not need to be included in its
    acknowledgment.

    Upon receipt of this acknowledgment, the local KDC issues a ticket
    to the client, encrypted with the shared key distributed using the
    above exchange.  This shared key can then be used for all subsequent
    cross-realm authentications to that particular remote KDC, for all
    clients, until the key expires.  The transited field of all such
    tickets should indicate, in addition to any realms transited, the
    certification path used to verify the local KDC's public key.

3.2.  Public Key Exchange

    In the event that the local KDC does not have the remote KDC's
    public key certificate, it must perform an exchange to obtain it.
    It begins by sending a message to the remote KDC, signed by the
    local KDC's private key:

        PKX-PUBLIC-KEY-SEND ::= [APPLICATION 33] SEQUENCE {
            signedListPack          [0] SignedListPack,
            pubKeyCert              [1] Certificate
        }

        SignedListPack ::= SEQUENCE {
            listPack                [0] ListPack,
            sigListPack             [1] Signature
                                        -- of listPack
        }

        ListPack ::= SEQUENCE {
            listTrust               [0] SEQUENCE OF OCTET STRING,
            timestamp               [1] KerberosTime,
            listPackNonce           [2] INTEGER
        }

    where listTrust is a list of certifiers trusted by the local KDC.

    Upon receipt of the PKX-PUBLIC-KEY-SEND, the remote KDC checks to
    see if its own public key is certified by one of the trusted
    certifiers.  If not, or if the signature is invalid, then it
    returns an error message.

    Otherwise, there are two cases: either the remote KDC trusts the
    local KDC's public key certificate, or it does not.  If it does
    not, then it sends back an error message with an e-data field of

        PKX-PUBLIC-KEY-ERROR ::= [APPLICATION 35] SEQUENCE {
            signedListPack          [0] SignedListPack,
            pubKeyCert              [1] Certificate
        }

    where the signedListPack contains a list of certifiers trusted
    by the *remote* KDC, and the pubKeyCert is the remote KDC's public
    key certificate, certified by one of the local KDC's trusted
    certifiers.  If the local KDC does not have a certificate signed
    by one of the remote KDC's trusted certifiers, then it returns an
    error message to the client.  Otherwise, it resends the
    PKX-PUBLIC-KEY-SEND, this time with a new public key certificate.

    If the remote KDC does trust the local KDC's public key, then it
    sends back an acknowledgment:

        PKX-PUBLIC-KEY-ACK ::= [APPLICATION 34] SEQUENCE {
            signedPubAck            [0] SignedPubAck,
            pubKeyCert              [1] Certificate
        }

        SignedPubAck ::= SEQUENCE {
            pubAckPack              [0] PubAckPack,
            sigPubAck               [1] Signature
                                        -- of pubAckPack
        }

        PubAckPack ::= SEQUENCE {
            timestamp               [0] KerberosTime,
            listPackNonce           [1] INTEGER,
                                        -- from listPack
            pubAckNonce             [2] INTEGER
                                        -- new nonce
        }

    Upon verification of the PKX-PUBLIC-KEY-ACK, the local KDC then
    proceeds with the basic exchange from Section 3.1.

4.  Finding Realms Supporting PK-CROSS

    If either the local realm or the destination realm does not support
    PK-CROSS, or both do not, the mechanism specified in Section 3 can
    still be used in obtaining the desired remote TGT.

    In the reference Kerberos implementations, the default behavior is
    to traverse a path up and down the realm name hierarchy, if the
    two realms do not share a key.  There is, however, the possibility
    of using cross links--i.e., keys shared between two realms that
    are non-contiguous in the realm name hierarchy--to shorten the
    path, both to minimize delay and the number of intermediate realms
    that need to be trusted.

    PK-CROSS can be used as a way to provide cross-links even in the
    absence of shared keys.  If the client is aware that one or two
    intermediate realms support PK-CROSS, then a combination of
    PK-CROSS and conventional cross-realm authentication can be used
    to reach the final destination realm.

    We solicit discussion on the best methods for clients and KDCs to
    determine or advertise support for PK-CROSS.

5.  Message Ports

    We have not specified the port on which KDCs supporting PK-CROSS
    should listen to receive the additional messages described above.
    We solicit discussion on which port should be used.  We propose to
    use the standard Kerberos ports (well-known 88 or 750), but another
    possibility is to use the kadmin port.

6.  Expiration Date

    This Internet-Draft will expire on or about May 25, 1997.

7.  Authors' Addresses

    Brian Tung
    USC/Information Sciences Institute
    4676 Admiralty Way Suite 1001
    Marina del Rey, CA 90292-6695

    Phone: 310-822-1511
    E-Mail: brian@isi.edu

    Tatyana Ryutov
    USC/Information Sciences Institute
    4676 Admiralty Way Suite 1001
    Marina del Rey, CA 90292-6695

    Phone: 310-822-1511
    E-Mail: tryutov@isi.edu

    Clifford Neuman
    USC/Information Sciences Institute
    4676 Admiralty Way Suite 1001
    Marina del Rey, CA 90292-6695

    Phone: 310-822-1511
    E-Mail: bcn@isi.edu
