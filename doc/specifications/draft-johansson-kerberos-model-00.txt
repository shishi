

Network Working Group                                          Johansson
Internet-Draft                                      Stockholm university
Expires: April 8, 2004                                   October 9, 2003


              An information model for Kerberos version 5
                     draft-johansson-kerberos-model-00

Status of this Memo

   This document is an Internet-Draft and is in full conformance with
   all provisions of Section 10 of RFC2026.

   Internet-Drafts are working documents of the Internet Engineering
   Task Force (IETF), its areas, and its working groups. Note that other
   groups may also distribute working documents as Internet-Drafts.

   Internet-Drafts are draft documents valid for a maximum of six months
   and may be updated, replaced, or obsoleted by other documents at any
   time. It is inappropriate to use Internet-Drafts as reference
   material or to cite them other than as "work in progress."

   The list of current Internet-Drafts can be accessed at http://
   www.ietf.org/ietf/1id-abstracts.txt.

   The list of Internet-Draft Shadow Directories can be accessed at
   http://www.ietf.org/shadow.html.

   This Internet-Draft will expire on April 8, 2004.

Copyright Notice

   Copyright (C) The Internet Society (2003). All Rights Reserved.

Abstract

   This document describes an information model for Kerberos version 5
   from the point of view of an administrative service. There is no
   standard for administrating a kerberos 5 KDC. This document describes
   the services exposed by an administrative interface to a KDC.












Johansson                Expires April 8, 2004                  [Page 1]

Internet-Draft    An information model for Kerberos version 5  October 2003


Table of Contents

   1.    Requirements notation  . . . . . . . . . . . . . . . . . . .  3
   2.    Motivation . . . . . . . . . . . . . . . . . . . . . . . . .  4
   3.    Acknowledgments  . . . . . . . . . . . . . . . . . . . . . .  5
   4.    Information model  . . . . . . . . . . . . . . . . . . . . .  6
   4.1   Principal  . . . . . . . . . . . . . . . . . . . . . . . . .  6
   4.1.1 Principal: Attributes  . . . . . . . . . . . . . . . . . . .  6
   4.1.2 Principal: Associations  . . . . . . . . . . . . . . . . . .  6
   4.1.3 Principal: Remarks . . . . . . . . . . . . . . . . . . . . .  6
   4.2   KeySet . . . . . . . . . . . . . . . . . . . . . . . . . . .  7
   4.2.1 KeySet: Attributes . . . . . . . . . . . . . . . . . . . . .  7
   4.2.2 KeySet: Associations . . . . . . . . . . . . . . . . . . . .  7
   4.2.3 KeySet: Remarks  . . . . . . . . . . . . . . . . . . . . . .  7
   4.3   Key  . . . . . . . . . . . . . . . . . . . . . . . . . . . .  7
   4.3.1 Key: Attributes  . . . . . . . . . . . . . . . . . . . . . .  7
   4.3.2 Key: Associations  . . . . . . . . . . . . . . . . . . . . .  8
   4.3.3 Key: Remarks . . . . . . . . . . . . . . . . . . . . . . . .  8
   4.4   Policy . . . . . . . . . . . . . . . . . . . . . . . . . . .  8
   4.4.1 Policy: Attributes . . . . . . . . . . . . . . . . . . . . .  8
   4.4.2 Password Quality Policy  . . . . . . . . . . . . . . . . . .  9
   4.4.3 Password Management Policy . . . . . . . . . . . . . . . . .  9
   4.4.4 Keying Policy  . . . . . . . . . . . . . . . . . . . . . . .  9
   5.    Implementation Scenarios . . . . . . . . . . . . . . . . . . 10
   5.1   LDAP backend to KDC  . . . . . . . . . . . . . . . . . . . . 10
   5.2   LDAP frontend to KDC . . . . . . . . . . . . . . . . . . . . 10
   5.3   SOAP . . . . . . . . . . . . . . . . . . . . . . . . . . . . 10
   6.    Security Considerations  . . . . . . . . . . . . . . . . . . 11
   7.    Remarks  . . . . . . . . . . . . . . . . . . . . . . . . . . 12
         References . . . . . . . . . . . . . . . . . . . . . . . . . 13
         Author's Address . . . . . . . . . . . . . . . . . . . . . . 13
         Intellectual Property and Copyright Statements . . . . . . . 14



















Johansson                Expires April 8, 2004                  [Page 2]

Internet-Draft    An information model for Kerberos version 5  October 2003


1. Requirements notation

   The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
   "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
   document are to be interpreted as described in [RFC2119].














































Johansson                Expires April 8, 2004                  [Page 3]

Internet-Draft    An information model for Kerberos version 5  October 2003


2. Motivation

   The Kerberos version 5 authentication service described in [RFC1510]
   describes how a Key Distribution Service (KDC) provides
   authentication to clients. The standard does not stipulate how a KDC
   is managed and several "kadmin" servers have evolved. This document
   describes the services required to administrate a KDC and the
   underlying information model assumed by a kadmin service.

   The information model is written in terms of "attributes" and
   "services" or "interfaces" but the use of these particular words MUST
   NOT be taken to imply any particular modeling paradigm so that
   neither an object oriented model or an LDAP schema is intended. The
   author has attempted to describe in natural language the intended
   semantics and syntax of the components of the model. An LDAP schema
   (say) based on this model will be more precise in the expression of
   the syntax while preserving the semantics of this model.

   Implementations of this document MAY decide to change the names used
   (eg principalName). If so an implementation MUST provide a name to
   name mapping to this document.






























Johansson                Expires April 8, 2004                  [Page 4]

Internet-Draft    An information model for Kerberos version 5  October 2003


3. Acknowledgments

   Love H�rnquist-�strand <lha@it.su.se> for important contributions.
















































Johansson                Expires April 8, 2004                  [Page 5]

Internet-Draft    An information model for Kerberos version 5  October 2003


4. Information model

4.1 Principal

   The fundamental entity stored in a KDC is the principal. The
   principal is associated to keys and generalizes the "user" concept.

4.1.1 Principal: Attributes

4.1.1.1 principalName

   A principal MUST be uniquely identified withing the administrative
   context of the KDC by the principalName. The type of the
   principalName is not described in this document. It is a unique
   identifier and can be viewed as an opaque which can be compared for
   equality with.

4.1.1.2 principalNotUsedBefore

   The principal may not be used before this date. The syntax of the
   attribute MUST be semantically equivalent with ISO date format.

4.1.1.3 principalNotUsedAfter

   The principal may not be used after this date. The syntax of the
   attribute MUST be semantically equivalent to standard ISO date
   format.

4.1.1.4 principalIsDisabled

   A boolean attribute used to temporarily disable a principal.

4.1.1.5 principalAliases

   This multivalued attribute contains a set of aliases for the
   principal.

4.1.2 Principal: Associations

   Each principal MUST be associated with exactly one KeySet and MAY be
   associated with 1 or more Policies. The KeySet is represented as an
   object in this model since it has attributes associated with it (the
   key version number).

4.1.3 Principal: Remarks

   Traditionally a principal consists of a local-part and a realm
   denoted in string form by local-part@REALM. The realm concept is used



Johansson                Expires April 8, 2004                  [Page 6]

Internet-Draft    An information model for Kerberos version 5  October 2003


   to provide administrative boundaries and together with cross-realm
   authentication provides scalability to Kerberos 5. However the realm
   is not central to an administrative information model. For instance
   the initialization or creation of a realm is equivalent to creating a
   specific set of principals (krbtgt@REALM, etc) which is covered by
   the model and services described in this document. A realm is
   typically associated with policy covering (for instance) keying and
   password management. The management of such policy and their
   association to realms is beyond the scope of this document.

4.2 KeySet

   A KeySet is a set of keys associated with exactly one principal. This
   object and the associated associations MAY be supported by
   implementations. Implementations (eg and LDAP schema) MUST NOT
   mandate the representation of keys. It is expected that most
   implementations of this standard will use the set/change password
   protocol for all aspects of key management
   [I-D.ietf-krb-wg-kerberos-set-passwd]. This information model
   includes these objects for completeness.

4.2.1 KeySet: Attributes

4.2.1.1 keySetVersionNumber

   This is traditionally called the key version number (kvno).

4.2.2 KeySet: Associations

   To each KeySet MUST be associated a set of 1 or more Keys.

4.2.3 KeySet: Remarks

   The reason for separating the KeySet from the Principal is security.
   The security of Kerberos 5 depends absolutely on the security of the
   keys stored in the KDC. The KeySet type is provided to make this
   clear and to make separation of keys from other parts of the model
   clear.

   Implementations of this standard (eg an LDAP schema) MUST preserve
   this distinction.

4.3 Key

4.3.1 Key: Attributes






Johansson                Expires April 8, 2004                  [Page 7]

Internet-Draft    An information model for Kerberos version 5  October 2003


4.3.1.1 keyEncryptionType

   TODO

4.3.1.2 keyValue

   TODO

4.3.1.3 keySaltValue

   TODO

4.3.1.4 keyStringToKeyParameter

   The syntax of this opaque object is defined by the encryption type
   used.

4.3.1.5 keyNotUsedAfter

   This key MUST NOT be used after this date.

4.3.1.6 keyNotUsedBefore

   This key MUST NOT be used before this date.

4.3.1.7 keyIsDisabled

   If this attribute is true the key MUST NOT be used

4.3.2 Key: Associations

   None

4.3.3 Key: Remarks

   The security of the keys is a requirement for the operation of
   Kerberos 5.

4.4 Policy

4.4.1 Policy: Attributes

4.4.1.1 policyIdentifier

   The policyIdentifier MUST be unique within the local administrative
   context and SHOULD be globally unique. It may be valuable to use a
   mechanism for policy identification which is likely to be
   representable in may possible implementations of this standard. A few



Johansson                Expires April 8, 2004                  [Page 8]

Internet-Draft    An information model for Kerberos version 5  October 2003


   possibilities are

      An Object Identifier (OID)

      A URN

      A UUID


4.4.1.2 policyIsMandatory

   This attribute indicates that the KDC MUST be able to correctly
   interpret and apply this policy for the key to be used.

4.4.1.3 policyContent

   This is an optional opaque binary value used to store a
   representation of the policy. In general a policy cannot be fully
   expressed using attribute-value pairs. The attribute is optional in
   the sense that an implementation MAY use it to store an opaque value
   for those policy-types which are not representable in that
   implementation. When used the attribute MUST be implemented as a
   mandatory attribute if the implementation supports mandatory/optional
   attributes.

4.4.2 Password Quality Policy

4.4.2.1 Password Quality Policy: Attributes

   Password quality policy controls the requirements placed by the KDC
   on new passwords. TODO: update with information from Nico

4.4.3 Password Management Policy

4.4.3.1 Password Management Policy: Attributes

   Password management policy controls how passwords are changed. TODO:
   update with information from Nico and Ludovic

4.4.4 Keying Policy

4.4.4.1 Keying Policy: Attributes

   A keying policy specifies the association of enctypes with new
   principals, i.e when a principal is created one of the possibly many
   applicable keying policies determine the set of keys to associate
   with the principal. In general the expression of a keying policy may
   require a Touring-complete language.



Johansson                Expires April 8, 2004                  [Page 9]

Internet-Draft    An information model for Kerberos version 5  October 2003


5. Implementation Scenarios

   There are several ways to implement an administrative service for
   Kerberos 5 based on this information model. In this section we list a
   few of them.

5.1 LDAP backend to KDC

   Given an LDAP schema implementation of this information model it
   would be possible to build an administrative service by backending
   the KDC to a directory server where principals and keys are stored.
   Using the security mechanisms available on the directory server keys
   are protected from access by anyone apart from the KDC.
   Administration of the principals, policy and other non-key data is
   done through the directory server while the keys are modified using
   the set/change password protocol
   [I-D.ietf-krb-wg-kerberos-set-passwd].

5.2 LDAP frontend to KDC

   An alternative way to provide a directory interface to the KDC is to
   implement an LDAP-frontend to the KDC which exposes all non-key
   objects as entries and attributes. As in the example above all keys
   are modified using the set/change password protocol
   [I-D.ietf-krb-wg-kerberos-set-passwd]. In this scenario the
   implementation would typically not use a traditional LDAP
   implementation but treat LDAP as an access-protocol to data in the
   native KDC database.

5.3 SOAP

   Given an XML schema implementation of this information model it would
   be possible to build a SOAP-interface to the KDC. This demonstrates
   the value of creating an abstract information model which is mappable
   to multiple schema representations.
















Johansson                Expires April 8, 2004                 [Page 10]

Internet-Draft    An information model for Kerberos version 5  October 2003


6. Security Considerations

   This document describes an abstract information model for Kerberos 5.
   The Kerberos 5 protocol depends on the security of the keys stored in
   the KDC. The model described here assumes that keys MUST NOT be
   transported in the clear over the network and furthermore that keys
   are treated as write-only attributes that SHALL only be modified
   (using the administrative interface) by the change-password protocol
   [I-D.ietf-krb-wg-kerberos-set-passwd].

   Exposing the object model of a KDC typically implies that objects can
   be modified and/or deleted. In a KDC not all principals are created
   equal, so that for instance deleting krbtgt/EXAMPLE.COM@EXAMPLE.COM
   effectively disables the EXAMPLE.COM realm. Hence access control is
   paramount to the security of any implementation. This document does
   not (at the time of writing - leifj) mandate access control. This
   only implies that access control is beyond the scope of the standard
   information model, i.e that access control MAY NOT be accessible via
   any protocol based on this model. If access control objects is
   exposed via an extension to this model the presence of access control
   may in itself provide points of attack by giving away information
   about principals with elevated rights etc. etc.





























Johansson                Expires April 8, 2004                 [Page 11]

Internet-Draft    An information model for Kerberos version 5  October 2003


7. Remarks

   A few notes and TODOs:

      Do we want to model access control? I have received a few notes on
      that from Love. It will affect both the model and the security
      considerations but It may be relevant. The catch is that most
      implementations (SOAP, LDAP, etc) will have acl mechanisms
      separate from the data which makes modeling acls difficult.
      Perhaps there are certain aspects of access control which can be
      modeled with relative ease - for instance the ability to make an
      object immutable.

      Need to specify how extensions to the model happens.

      Comparison for equality is implied by specifying syntax, but what
      about ordering? Do we need/want to compare objects/
      attribute-values for order?

      Explanatory text on a few of the basic attributes that doesn't
      just repeat the section title.

      Expand on the password policy types. Is the subdivision into
      quality and management policies valid?



























Johansson                Expires April 8, 2004                 [Page 12]

Internet-Draft    An information model for Kerberos version 5  October 2003


References

   [I-D.ietf-krb-wg-kerberos-clarifications]
              Neuman, C., "The Kerberos Network Authentication Service
              (V5)", draft-ietf-krb-wg-kerberos-clarifications-04 (work
              in progress), June 2003.

   [I-D.ietf-krb-wg-kerberos-set-passwd]
              Trostle, J., "Kerberos Set/Change Password: Version 2",
              draft-ietf-krb-wg-kerberos-set-passwd-00 (work in
              progress), May 2003.

   [RFC1510]  Kohl, J. and B. Neuman, "The Kerberos Network
              Authentication Service (V5)", RFC 1510, September 1993.

   [RFC2119]  Bradner, S., "Key words for use in RFCs to Indicate
              Requirement Levels", BCP 14, RFC 2119, March 1997.


Author's Address

   Leif Johansson
   Stockholm university
   Enheten f�r IT och Media
   Stockholm  SE-106 91

   EMail: leifj@it.su.se
   URI:   http://people.su.se/~leifj/























Johansson                Expires April 8, 2004                 [Page 13]

Internet-Draft    An information model for Kerberos version 5  October 2003


Intellectual Property Statement

   The IETF takes no position regarding the validity or scope of any
   intellectual property or other rights that might be claimed to
   pertain to the implementation or use of the technology described in
   this document or the extent to which any license under such rights
   might or might not be available; neither does it represent that it
   has made any effort to identify any such rights. Information on the
   IETF's procedures with respect to rights in standards-track and
   standards-related documentation can be found in BCP-11. Copies of
   claims of rights made available for publication and any assurances of
   licenses to be made available, or the result of an attempt made to
   obtain a general license or permission for the use of such
   proprietary rights by implementors or users of this specification can
   be obtained from the IETF Secretariat.

   The IETF invites any interested party to bring to its attention any
   copyrights, patents or patent applications, or other proprietary
   rights which may cover technology that may be required to practice
   this standard. Please address the information to the IETF Executive
   Director.


Full Copyright Statement

   Copyright (C) The Internet Society (2003). All Rights Reserved.

   This document and translations of it may be copied and furnished to
   others, and derivative works that comment on or otherwise explain it
   or assist in its implementation may be prepared, copied, published
   and distributed, in whole or in part, without restriction of any
   kind, provided that the above copyright notice and this paragraph are
   included on all such copies and derivative works. However, this
   document itself may not be modified in any way, such as by removing
   the copyright notice or references to the Internet Society or other
   Internet organizations, except as needed for the purpose of
   developing Internet standards in which case the procedures for
   copyrights defined in the Internet Standards process must be
   followed, or as required to translate it into languages other than
   English.

   The limited permissions granted above are perpetual and will not be
   revoked by the Internet Society or its successors or assignees.

   This document and the information contained herein is provided on an
   "AS IS" basis and THE INTERNET SOCIETY AND THE INTERNET ENGINEERING
   TASK FORCE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
   BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION



Johansson                Expires April 8, 2004                 [Page 14]

Internet-Draft    An information model for Kerberos version 5  October 2003


   HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF
   MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.


Acknowledgement

   Funding for the RFC Editor function is currently provided by the
   Internet Society.











































Johansson                Expires April 8, 2004                 [Page 15]

