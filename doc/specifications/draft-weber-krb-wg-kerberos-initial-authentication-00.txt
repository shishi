


INTERNET DRAFT                                         Joel N. Weber II
                                                          June 22, 2003
                                              Expires December 22, 2003


                Kerberos Initial Authentication Methods
       draft-weber-krb-wg-kerberos-initial-authentication-00.txt


   This document is an Internet-Draft and is subject to all the
   provisions of Section 10 of RFC2026.

   Internet-Drafts are working documents of the Internet Engineering
   Task Force (IETF), its areas, and its working groups.  Note that
   other groups may also distribute working documents as Internet-
   Drafts.

   Internet-Drafts are draft documents valid for a maximum of six months
   and may be updated, replaced, or obsoleted by other documents at any
   time.  It is inappropriate to use Internet-Drafts as reference
   material or to cite them other than as "work in progress."

   The list of current Internet-Drafts can be accessed at
   http://www.ietf.org/1id-abstracts.html

   The list of Internet-Draft Shadow Directories can be accessed at
   http://www.ietf.org/shadow.html

   The distribution of this memo is unlimited.  It is filed as draft-
   weber-krb-wg-kerberos-initial-authentication-00.txt, and expires 22
   December 2003.  Please send comments to the author or to ietf-krb-
   wg@anl.gov

ABSTRACT

   This document discusses the various methods that have been used for
   initial authentication in Kerberos, using passwords and various other
   forms of key material, as well as various methods that may offer
   significant benefits which may justify the effort of specifying and
   implementing them, as well as the advantages and disadvantages of the
   various approaches relative to each other.

INTRODUCTION

   Kerberos [KERBEROS] traditionally uses the principal's password as
   the only source of entropy for the encryption key used in initial
   authentication.




Weber            Kerberos Initial Authentication Methods        [Page 1]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   As key sizes have become larger, it has become clear that users are
   unlikely to type passwords long enough to have enough entropy to make
   full use of the key space offered by the newer encryption algorithms
   supported in Kerberos.

   Even ignoring the significant problem of educating users about
   generating quality passwords, memorizing and typing a password which
   truely has 128 bits or more of entropy is difficult.  [SCHNEIER96]
   suggests that entropy of English text can vary from 1.0 to 2.3 bits
   per character depending on the amount of text being used; it's not
   clear how much entropy gets added by typical methods of generating
   passwords used by humans such randomizing capitalization and
   substituting in numbers and punctuation.

   Because Kerberos essentially uses passwords directly as encryption
   keys, it is more vulnerable to weak passwords than most other
   authentication systems that attempt to protect against eavesdropping.

   There are two kinds of dictionary attack possible.  In an on-line
   dictionary attack, the attacker simply sends the KDC a request,
   asserting that the password is its first guess, and if that fails,
   tries a second, etc.  In an off-line dictionary attack, the attacker
   somehow gets a piece of ciphertext which is encrypted using the
   password, and tries decrypting the ciphertext repeatedly using
   different keys.

   There is no single scheme which is ideal for handling initial
   authentication for all sites using Kerberos.  Some sites want a
   method that will minimize the vulnerability to eavesdropping and man
   in the middle attacks when using only passwords.  Other sites are
   willing to deal with the extra effort required to make use of a
   stronger authentication method.

   This document may be useful to members of the Kerberos working group
   in understanding the issues related to initial authentication, and it
   is also hoped that it will eventually be published as an
   informational RFC to explain to administrators of Kerberos realms the
   tradeoffs between various initial authentication methods they might
   choose to use.

PASSWORDS

 Un-Preauthenticated Password Usage

   In the most basic protocol, the client tells the KDC the name of its
   principal, and the KDC sends back ciphertext encrypted in the
   principal's long term key.  This has the property that anyone who can
   contact the KDC can get a piece of ciphertext with which to mount an



Weber            Kerberos Initial Authentication Methods        [Page 2]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   off-line dictionary attack.  Widely deployed Kerberos implementations
   support this method, but generally allow it to be disabled.

 PA-ENC-TIMESTAMP Preauthentication

   With the PA-ENC-TIMESTAMP method, defined in [KERBEROS] and currently
   widely available in deployed Kerberos implementations, the client
   uses the principal's long term key to encrypt a message containing
   the current time, and sends this to the KDC; the KDC then sends back
   ciphertext encrypted in the principal's long term key.  This has the
   property that anyone who can eavesdrop the communication between the
   KDC and the client can get ciphertext encrypted in the user's long-
   term key, but getting such ciphertext requires eavesdropping.  Anyone
   who can contact the KDC can conduct an on-line dictionary attack.

 Diffie-Hellman + PA-ENC-TIMESTAMP

   A mechanism could be defined which would do an anonymous Diffie-
   Hellman exchange, followed by doing the PA-ENC-TIMESTAMP exchange
   encrypted using a key derived from the Diffie-Hellman exchange.  This
   would prevent an eavesdropper from gaining ciphertext for an off-line
   dictionary attack, but the man-in-the-middle attack case is still
   problematic.

   This has the advantage that an attacker who could do a successful PA-
   ENC-TIMESTAMP attack but is incapable of spoofing a kdc for the DH
   exchange would not be able to learn the user's password.  While it is
   possible that there may be some network topologies for which this is
   true, the author is somewhat unclear on what these topologies are.

   It also has the advantage that the attacker is more likely to be
   noticed.  Many network environments, such as unswitched wired
   Ethernet, wireless Ethernet, and perhaps cable modems, allow
   completely passive eavesdropping.  For example, using arp to
   intercept traffic to conduct a man-in-the-middle attack is likely to
   result in messages being logged about overwritten arp cache entries.

   A successful man-in-the-middle attack can prevent the kdc from ever
   noticing that the client had attempted to get credentials, and that
   only the client can reliably detect such an attack.  However, a
   mechanism involving the client using a cookie which reused for
   successive authentication attempts until authentication is
   successful, along with a count and list of principals tried
   maintained both by the client host and the KDC could be used to
   detect such man-in-the-middle attacks when they are intermittent, and
   a non-intermittent man-in-the-middle attack will be noticed by the
   failure of the workstation to usefully authenticate to the KDC.




Weber            Kerberos Initial Authentication Methods        [Page 3]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


 Fortified Key Negotiation

   Fortified Key Negotiation, as described in [SCHNEIER96], could be
   used to protect the initial Kerberos exchange.  From the user's
   perspective, Fortified Key Negotiation is indistinguishable from the
   DH + PA-ENC-TIMESTAMP approach.  The advantage of Fortified Key
   Negotiation is that an attacker will probably have to execute
   multiple denial of service attacks in order to learn enough about the
   password to conduct an off-line dictionary attack.

   Fortified Key Negotiation works by doing a Diffie-Hellman exchanged,
   and then leaking only some of the entropy of the key.  If the server
   that the client has connected to can demonstrate that it knows what
   the leaked bits are, then the client sends ciphertext which is
   sufficient for an off-line dictionary attack.  Alternatively,
   collecting the initial leaked entropy several times may provide
   enough information for an off-line dictionary attack.

   For example, if fortified key negotiation is being used and is
   leaking 12 bits at the begining of each connection, and the AES
   string-to-key function is being used with an iteration count that
   makes it take 1 second on a machine that runs at one eighth the speed
   of the fastest available machine, and the client waits 8 seconds
   before timing out, a single fastest available machine as a man-in-
   the-middle will have a one in 64 chance of successfully brute forcing
   the leaked entropy to send a reply before the client times out.  If
   the machine conducting the man-in-the-middle attack can communicate
   with a collection of 15 other equally fast machines, then it has a
   one in four chance of successfully cracking the leaked entropy before
   the connection times out.  And a handful of man in the middle attacks
   against the same principal is likely to reveal enough entropy for a
   successful off-line dictionary attack.

   It is not clear whether Fortified Key Negotiation provides benefit
   that justifies the extra work involved in specifying and implementing
   a wire protocol for it.

 EKE, SPEKE and SRP

   [EKE], [SPEKE], and [SRP] are examples of protocols which can solve
   some of the problems with the password schemes described above.
   However, as of the time of this writing, their intellectual property
   status is somewhat unclear.

 Third Party Encryption

   [DCERFC26] and [EXTRATGT] describe a way to use a host key to protect
   a weak password.  This has the disadvantage that it requires a host



Weber            Kerberos Initial Authentication Methods        [Page 4]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   key to be available in order to function, which increases the amount
   of setup required, especially environments where client computers are
   managed by different people than the administrators of the Kerberos
   realm.  On the other hand, it does prevent off-line dictionary
   attacks, and some sites may find it useful to configure their KDC to
   require a host key, so as to prevent on-line dictionary attacks from
   outside the organization.

 Public Key Encryption

   If a public key of the KDC can be distributed in a secure fashion, it
   is possible to use a scheme such as PA-ENC-TIMESTAMP and encrypt that
   using a public encryption key.

   The challenge lies in distributing the public key.  However, this is
   solvable in some cases.  For example, some large sites have
   mechanisms for distributing software updates to workstations in an
   automated fashion, and it is not clear that there is any reason why
   they couldn't use that mechanism to distribute the public key.  All
   users have to get their the password for their principal somehow when
   it is first created, and it is not entirely clear that there is a
   good reason why they could not be given a public key fingerprint at
   that time.  (Admittedly, some will manage to lose the piece of paper
   they are given, this may not help with people who first got their
   principal ten years ago, etc.)

   Another difficult question is what happens when the public/private
   keypair needs to be changed.

SINGLE USE AUTHENTICATION METHODS

   [SAM] discusses the use of single use authentication methods, both
   hardware tokens, and methods such as S/Key which are typically used
   with a piece of paper with a list one of time passwords.  This can
   help to counteract a tendancy to generate weak passwords, and the
   single use nature counteracts certain sorts of attacks involving
   keystroke recorders.

   Many of the methods used with SAM are weak in the same way that
   typical human-generated passwords are weak, and it is desireable for
   anonymous Diffie-Hellman and for third party encryption to be usable
   in conjunction with SAM.

CRYPTOGRAPHIC TOKENS

   Cryptographic tokens exist which will perform cryptographic
   operations but not divulge the key.  For example, several companies
   sell devices which plug into a USB port which will do 1024 bit RSA



Weber            Kerberos Initial Authentication Methods        [Page 5]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   operations.  Some of these devices also support 2048 bit RSA.  At
   least one of these devices also supports AES.  The typical selling
   price of these devices is a small fraction of the cost of a new
   computer.

   It appears that the Kerberos wire protocol for getting tickets using
   a service key stored in a file on a hard drive will also be
   sufficient for using symmetric key encryption with one of these
   devices, although new enctypes may need to be defined.

   [PKINIT] ought to provide an appropriate framework for using these
   devices in public key mode; whether changes will be required has not
   been investigated by the author of this document.

HIDING PRINCIPAL NAMES

   There are some who believe that sending principal names in the clear
   has the potential to be a security problem.  If anonymous Diffie-
   Hellman and the third party encryption schemes are defined in such a
   way that they encrypt the entire ticket request and reply rather than
   only the preauthentication field, this potential vulnerability is
   minimized.

SECURITY CONSIDERATIONS

   This entire document is about security considerations.

ACKNOWLEDGEMENTS

   Much of the information in this document came from the ietf-krb-wg
   mailing list archive, especially from mail from early December 2002.
   Many people contributed to that discussion, and an attempt to
   determine who the significant contributors are would doubtless
   inadvertently omit a few people.

AUTHOR'S ADDRESS

   Joel N. Weber II
   185 Lowell St #2
   Somerville MA 02144-2629
   Email: weber@joelweber.com


NORMATIVE REFERENCES

   [KERBEROS] RFC-Editor: To be replaced by RFC number for draft-ietf-
   krb-wg-kerberos-clarifications.




Weber            Kerberos Initial Authentication Methods        [Page 6]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   [RFC2026] S. Bradner, RFC2026, BCP 9:  "The Internet Standard Process
   - Revision 3," October 1996, Obsoletes - RFC 1602, Status: Best
   Current Practice.

   [RFC2119] S. Bradner, RFC 2119, BCP 14: "Key words for use in RFCs to
   Indicate Requirement Levels", March 1997.

INFORMATIVE REFERENCES

   [DCERFC26] J. Pato, DCE RFC 26: "Using Pre-Authentication to Avoid
   Password Guessing Attacks'', June 1993.

   [EKE] S. Bellovin, M. Merritt, "Encrypted Key Exchange: Password-
   based protocols secure against dictionary attacks", Proceedings of
   the IEEE Symposium on Research in Security and Privacy, May 1992.

   [EXTRATGT] J. Trostle, M. Swift: "Extension to Kerberos V5 For
   Additional Initial Encryption".  Unfinished work in progress, expired
   January 30, 2000.

   [PKINIT]

   [SAM] C. Neuman, G. Zorn, K. Hornstein, K. Renard: "Integrating
   Single-use Authentication Mechanisms with Kerberos", work in
   progress.

   [SCHNEIER96] B. Schneier, "Applied Cryptography", 1996.

   [SPEKE] D. Jablon, "Strong Password-Only Authenticated Key Exchange",
   Computer Communication Review, ACM SIGCOMM, vol. 26, no. 5, pp. 5-26,
   October 1996.

   [SRP] T. Wu, "The Secure Remote Password Protocol", ISOC NDSS
   Symposium, 1998.


IPR NOTICES

   The IETF has been notified of intellectual property rights claimed in
   regard to some or all of the specification contained in this
   document.  For more information consult the online list of claimed
   rights.

   The IETF takes no position regarding the validity or scope of any
   intellectual property or other rights that might be claimed to
   pertain to the implementation or use of the technology described in
   this document or the extent to which any license under such rights
   might or might not be available; neither does it represent that it



Weber            Kerberos Initial Authentication Methods        [Page 7]






draft-weber-krb-wg-kerberos-initial-authentication-00.txt   June 22 2003


   has made any effort to identify any such rights.  Information on the
   IETF's procedures with respect to rights in standards-track and
   standards-related documentation can be found in BCP-11.  Copies of
   claims of rights made available for publication and any assurances of
   licenses to be made available, or the result of an attempt made to
   obtain a general license or permission for the use of such
   proprietary rights by implementors or users of this specification can
   be obtained from the IETF Secretariat.

   The IETF invites any interested party to bring to its attention any
   copyrights, patents or patent applications, or other proprietary
   rights which may cover technology that may be required to practice
   this standard.  Please address the information to the IETF Executive
   Director.

COPYRIGHT NOTICE

   Copyright (C) The Internet Society 2003. All Rights Reserved.

   This document and translations of it may be copied and furnished to
   others, and derivative works that comment on or otherwise explain it
   or assist in its implmentation may be prepared, copied, published and
   distributed, in whole or in part, without restriction of any kind,
   provided that the above copyright notice and this paragraph are
   included on all such copies and derivative works.  However, this
   document itself may not be modified in any way, such as by removing
   the copyright notice or references to the Internet Society or other
   Internet organizations, except as needed for the purpose of
   developing Internet standards in which case the procedures for
   copyrights defined in the Internet Standards process must be
   followed, or as required to translate it into languages other than
   English.

   The limited permissions granted above are perpetual and will not be
   revoked by the Internet Society or its successors or assigns.

   This document and the information contained herein is provided on an
   "AS IS" basis and THE INTERNET SOCIETY AND THE INTERNET ENGINEERING
   TASK FORCE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
   BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION
   HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF
   MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.









Weber            Kerberos Initial Authentication Methods        [Page 8]



