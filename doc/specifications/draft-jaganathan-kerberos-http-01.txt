


Internet Engineering Task Force                            K. Jaganathan
Internet-Draft                                                    L. Zhu
Document: draft-jaganathan-kerberos-http-01.txt                J. Brezak
Category: Informational                            Microsoft Corporation
Expires: January 19, 2006                                  July 18, 2005


               Kerberos based HTTP Authentication in Windows
                 draft-jaganathan-kerberos-http-01.txt

Status of this Memo

   By submitting this Internet-Draft, each author represents that any
   applicable patent or other IPR claims of which he or she is aware
   have been or will be disclosed, and any of which he or she becomes
   aware will be disclosed, in accordance with Section 6 of BCP 79.

   Internet-Drafts are working documents of the Internet Engineering
   Task Force (IETF), its areas, and its working groups.  Note that
   other groups may also distribute working documents as Internet-
   Drafts.

   Internet-Drafts are draft documents valid for a maximum of six months
   and may be updated, replaced, or obsoleted by other documents at any
   time.  It is inappropriate to use Internet-Drafts as reference
   material or to cite them other than as "work in progress."

   The list of current Internet-Drafts can be accessed at
   http://www.ietf.org/ietf/1id-abstracts.txt.

   The list of Internet-Draft Shadow Directories can be accessed at
   http://www.ietf.org/shadow.html.

   This Internet-Draft will expire on January 19, 2006.

Copyright Notice

   Copyright (C) The Internet Society (2005).

Abstract

   This document describes how the Microsoft Internet Explorer (MSIE)
   and Internet Information Services (IIS) incorporated in Microsoft
   Windows 2000 use Kerberos for security enhancements of web
   transactions.  The Hypertext Transport Protocol (HTTP) auth-scheme of
   "negotiate" is defined here; when the negotiation results in the
   selection of Kerberos, the security services of authentication and
   optionally impersonation(the IIS server assuming the windows identity



Jaganathan, et al.      Expires January 19, 2006                [Page 1]

Internet-Draft       HTTP Authentication in Windows            July 2005


   of the principal which has been authenticated) are performed.  This
   document explains how HTTP authentication utilizes the Simple and
   Protected GSS-API Negotiation mechanism.  Details of SPNEGO
   implementation are not provided in this document.

Table of Contents

   1.  Introduction . . . . . . . . . . . . . . . . . . . . . . . . .  3
   2.  Conventions Used in This Document  . . . . . . . . . . . . . .  4
   3.  Access Authentication  . . . . . . . . . . . . . . . . . . . .  5
     3.1   Reliance on the HTTP/1.1 Specification . . . . . . . . . .  5
   4.  HTTP Negotiate Authentication Scheme . . . . . . . . . . . . .  6
     4.1   The WWW-Authenticate Response Header . . . . . . . . . . .  6
     4.2   The Authorization Request Header . . . . . . . . . . . . .  7
   5.  Negotiate Operation Example  . . . . . . . . . . . . . . . . .  8
   6.  Security Considerations  . . . . . . . . . . . . . . . . . . . 10
   7.  Normative References . . . . . . . . . . . . . . . . . . . . . 10
       Authors' Addresses . . . . . . . . . . . . . . . . . . . . . . 11
       Intellectual Property and Copyright Statements . . . . . . . . 12
































Jaganathan, et al.      Expires January 19, 2006                [Page 2]

Internet-Draft       HTTP Authentication in Windows            July 2005


1.  Introduction

   Microsoft has provided support for Kerberos authentication in MSIE
   and IIS in addition to other mechanisms.  This provides the benefits
   of the Kerberos v5 protocol for Web applications.  Support for
   Kerberos authentication is based on other previously defined
   mechanisms such as SPNEGO and the Generic Security Services
   Application Program Interface(GSSAPI).











































Jaganathan, et al.      Expires January 19, 2006                [Page 3]

Internet-Draft       HTTP Authentication in Windows            July 2005


2.  Conventions Used in This Document

   The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
   "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
   document are to be interpreted as described in [RFC2119].














































Jaganathan, et al.      Expires January 19, 2006                [Page 4]

Internet-Draft       HTTP Authentication in Windows            July 2005


3.  Access Authentication

3.1  Reliance on the HTTP/1.1 Specification

   This specification is a companion to the HTTP/1.1 specification
   [RFC2616] and builds on the authentication mechanisms defined in
   [RFC2617].  It uses the augmented BNF section 2.1 of that document,
   and relies on both the non-terminals defined in that document and
   other aspects of the HTTP/1.1 specification.










































Jaganathan, et al.      Expires January 19, 2006                [Page 5]

Internet-Draft       HTTP Authentication in Windows            July 2005


4.  HTTP Negotiate Authentication Scheme

   Use of Kerberos is wrapped in an HTTP auth-scheme of "Negotiate".
   The auth-params exchanged use data formats defined for use with the
   GSS-API [RFC2078].  In particular, they follow the formats set for
   the SPNEGO [RFC2478] and Kerberos [RFC4121] mechanisms for GSSAPI.
   The "Negotiate" auth-scheme calls for the use of SPNEGO GSSAPI tokens
   which the specific mechanism type specifies.

   The current implementation of this protocol is limited to the use of
   SPNEGO with the Kerberos and Microsoft(NT Lan Manager) NTLM
   protocols.

4.1  The WWW-Authenticate Response Header

   If the server receives a request for an access-protected object, and
   an acceptable Authorization header has not been sent, the server
   responds with a "401 Unauthorized" status code, and a "WWW-
   Authenticate:" header as per the framework described in [RFC2616].
   The initial WWW-Authenticate header will not carry any gssapi-data.

   The negotiate scheme will operate as follows:


           challenge       = "Negotiate" auth-data
           auth-data       = 1#( [gssapi-data] )

   The meanings of the values of the directives used above are as
   follows:

   gssapi-data

   If the gss_accept_security_context return a token for the client,
   this directive contains the base64 encoding of an InitialContextToken
   as defined in [RFC2078].  This is not present in the initial response
   from the server.

   A status code 200 status response can also carry a "WWW-
   Authenticate" response header containing the final leg of an
   authentication.  In this case, the gssapi-data will be present.
   Before using the contents of the response, the gssapi-data should be
   processed by gss_init_security_context to determine the state of the
   security context.  If this function indicates success, the response
   can be used by the application.  Otherwise an appropriate action
   based on the authentication status should be.

   For example the authentication could have failed on the final leg if
   mutual authentication was requested and the server was not able to



Jaganathan, et al.      Expires January 19, 2006                [Page 6]

Internet-Draft       HTTP Authentication in Windows            July 2005


   prove its identity.  In this case, the returned results are suspect.
   It is not always possible to mutually authenticate the server before
   the HTTP operation.  POST methods are in this category.

   When the Kerberos Version 5 GSSAPI mechanism [RFC4121] is being used,
   the HTTP server will be using a principal name of the form of "HTTP/
   hostname".

4.2  The Authorization Request Header

   Upon receipt of the response containing a "WWW-Authenticate" header
   from the server, the client is expected to retry the HTTP request,
   passing a HTTP "Authorization" header line.  This is defined
   according to the framework described in [RFC2616] utilized as
   follows:

            credentials             = "Negotiate" auth-data2
           auth-data2              = 1#( gssapi-data )

   gssapi-data

   This directive contains is the base64 encoding of an
   InitialContextToken as defined in [RFC2078].

   Any returned code other than a success 2xx code represents an
   authentication error.  If a 401 containing a "WWW-Authenticate"
   header with "Negotiate" and gssapi-data is returned from the server,
   it is a continuation of the authentication request.

   A client may initiate a connection to the server with an
   "Authorization" header containing the initial token for the server.
   This form will bypass the initial 401 error from the server when the
   client knows that the server will accept the Negotiate HTTP
   authentication type.

















Jaganathan, et al.      Expires January 19, 2006                [Page 7]

Internet-Draft       HTTP Authentication in Windows            July 2005


5.  Negotiate Operation Example

   The client requests an access-protected document from server via a
   GET method request.  The URI of the document is
   "http://www.nowhere.org/dir/index.html".


           C: GET dir/index.html

   The first time the client requests the document, no Authorization
   header is sent, so the server responds with:

           S: HTTP/1.1 401 Unauthorized
           S: WWW-Authenticate: Negotiate

   The client will obtain the user credentials using the SPNEGO GSSAPI
   mechanism type to identify generate a GSSAPI message to be sent to
   the server with a new request, including the following Authorization
   header:

            C: GET dir/index.html
           C: Authorization: Negotiate a87421000492aa874209af8bc028

   The server will decode the gssapi-data and pass this to the SPNEGO
   GSSAPI mechanism in the gss_accept_security_context function.  If the
   context is not complete, the server will respond with a 401 status
   code with a WWW-Authenticate header containing the gssapi-data.

           S: HTTP/1.1 401 Unauthorized
           S: WWW-Authenticate: Negotiate 749efa7b23409c20b92356

   The client will decode the gssapi-data and pass this into
   gss_init_security_context and return the new gssapi-data to the
   server.



           C: GET dir/index.html
           C: Authorization: Negotiate 89a8742aa8729a8b028


   This cycle can continue until the security context is complete.  When
   the return value from the gss_accept_security_context function
   indicates that the security context is complete, it may supply final
   authentication data to be returned to the client.  If the server has
   more gssapi data to send to the client to complete the context it is
   to be carried in WWW-Authenticate header with the final response
   containing the HTTP body.



Jaganathan, et al.      Expires January 19, 2006                [Page 8]

Internet-Draft       HTTP Authentication in Windows            July 2005


           S: HTTP/1.1 200 Success
           S: WWW-Authenticate: Negotiate ade0234568a4209af8bc0280289eca

   The client will decode the gssapi-data and supply it to
   gss_init_security_context using the context for this server.  If the
   status is successful from the final gss_init_security_context, the
   response can be used by the application.












































Jaganathan, et al.      Expires January 19, 2006                [Page 9]

Internet-Draft       HTTP Authentication in Windows            July 2005


6.  Security Considerations

   The SPNEGO HTTP authentication facility is only used to provide
   authentication of a user to server.  It provides no facilities for
   protecting the HTTP headers or data including the Authorization and
   WWW-Authenticate headers that are used to implement this mechanism.

   Alternate mechanisms such as TLS can be used to provide
   confidentiality.  Hashes of the TLS certificates can be used as
   channel bindings to secure the channel.  In this case clients would
   need to enforce that the channel binding information is valid.  Note
   that Kerb-TLS [RFC2712] could be used to provide both authentication
   and confidentiality but this requires a change to the TLS provider.

   This mechanism is not used for HTTP authentication to HTTP proxies.

   If an HTTP proxy is used between the client and server, it must take
   care to not share authenticated connections between different
   authenticated clients to the same server.  If this is not honored,
   then the server can easily lose track of security context
   associations.  A proxy that correctly honors client to server
   authentication integrity will supply the "Proxy-support: Session-
   Based-Authentication" HTTP header to the client in HTTP responses
   from the proxy.  The client MUST NOT utilize the SPNEGO HTTP
   authentication mechanism through a proxy unless the proxy supplies
   this header with the "401 Unauthorized" response from the server.

   When using the SPNEGO HTTP authentication facility with client
   supplied data such as PUT and POST, the authentication should be
   complete between the client and server before sending the user data.
   The return status from the gss_init_security_context will indicate
   with the security context is complete.  At this point the data can be
   sent to the server.

7.  Normative References

   [RFC2078]  Linn, J., "Generic Security Service Application Program
              Interface, Version 2", RFC 2078, January 1997.

   [RFC2119]  Bradner, S., "Key words for use in RFCs to Indicate
              Requirement Levels", BCP 14, RFC 2119, March 1997.

   [RFC2478]  Baize, E. and D. Pinkas, "The Simple and Protected GSS-API
              Negotiation Mechanism", RFC 2478, December 1998.

   [RFC2616]  Fielding, R., Gettys, J., Mogul, J., Frystyk, H.,
              Masinter, L., Leach, P., and T. Berners-Lee, "Hypertext
              Transfer Protocol -- HTTP/1.1", RFC 2616, June 1999.



Jaganathan, et al.      Expires January 19, 2006               [Page 10]

Internet-Draft       HTTP Authentication in Windows            July 2005


   [RFC2617]  Franks, J., Hallam-Baker, P., Hostetler, J., Lawrence, S.,
              Leach, P., Luotonen, A., and L. Stewart, "HTTP
              Authentication: Basic and Digest Access Authentication",
              RFC 2617, June 1999.

   [RFC2712]  Medvinsky, A. and M. Hur, "Addition of Kerberos Cipher
              Suites to Transport Layer Security (TLS)", RFC 2712,
              October 1999.

   [RFC4120]  Neuman, C., Yu, T., Hartman, S., and K. Raeburn, "The
              Kerberos Network Authentication Service (V5)", RFC 4120,
              July 2005.

   [RFC4121]  Zhu, L., Jaganathan, K., and S. Hartman, "The Kerberos
              Version 5 Generic Security Service Application Program
              Interface (GSS-API) Mechanism: Version 2", RFC 4121,
              July 2005.


Authors' Addresses

   Karthik Jaganathan
   Microsoft Corporation
   One Microsoft Way
   Redmond, WA  98052
   US

   Email: karthikj@microsoft.com


   Larry Zhu
   Microsoft Corporation
   One Microsoft Way
   Redmond, WA  98052
   US

   Email: lzhu@microsoft.com


   John Brezak
   Microsoft Corporation
   One Microsoft Way
   Redmond, WA  98052
   US

   Email: jbrezak@microsoft.com





Jaganathan, et al.      Expires January 19, 2006               [Page 11]

Internet-Draft       HTTP Authentication in Windows            July 2005


Intellectual Property Statement

   The IETF takes no position regarding the validity or scope of any
   Intellectual Property Rights or other rights that might be claimed to
   pertain to the implementation or use of the technology described in
   this document or the extent to which any license under such rights
   might or might not be available; nor does it represent that it has
   made any independent effort to identify any such rights.  Information
   on the procedures with respect to rights in RFC documents can be
   found in BCP 78 and BCP 79.

   Copies of IPR disclosures made to the IETF Secretariat and any
   assurances of licenses to be made available, or the result of an
   attempt made to obtain a general license or permission for the use of
   such proprietary rights by implementers or users of this
   specification can be obtained from the IETF on-line IPR repository at
   http://www.ietf.org/ipr.

   The IETF invites any interested party to bring to its attention any
   copyrights, patents or patent applications, or other proprietary
   rights that may cover technology that may be required to implement
   this standard.  Please address the information to the IETF at
   ietf-ipr@ietf.org.


Disclaimer of Validity

   This document and the information contained herein are provided on an
   "AS IS" basis and THE CONTRIBUTOR, THE ORGANIZATION HE/SHE REPRESENTS
   OR IS SPONSORED BY (IF ANY), THE INTERNET SOCIETY AND THE INTERNET
   ENGINEERING TASK FORCE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED,
   INCLUDING BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE
   INFORMATION HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED
   WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.


Copyright Statement

   Copyright (C) The Internet Society (2005).  This document is subject
   to the rights, licenses and restrictions contained in BCP 78, and
   except as set forth therein, the authors retain all their rights.


Acknowledgment

   Funding for the RFC Editor function is currently provided by the
   Internet Society.




Jaganathan, et al.      Expires January 19, 2006               [Page 12]


